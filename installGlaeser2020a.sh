#!/bin/sh

# dune-common
# releases/2.6 # f3cb9408388f3424b4f0a7a55cad84057b6b444d # 2018-12-13 17:16:34 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.6
git reset --hard f3cb9408388f3424b4f0a7a55cad84057b6b444d
cd ..

# dune-geometry
# releases/2.6 # 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38 # 2018-12-06 22:56:21 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.6
git reset --hard 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38
cd ..

# dune-grid
# releases/2.6 # 76f18471498824d49a6cecbfba520b221d9f79ca # 2018-12-10 14:35:11 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.6
git reset --hard 76f18471498824d49a6cecbfba520b221d9f79ca
cd ..

# dune-localfunctions
# releases/2.6 # ee794bfdfa3d4f674664b4155b6b2df36649435f # 2018-12-06 23:40:32 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.6
git reset --hard ee794bfdfa3d4f674664b4155b6b2df36649435f
cd ..

# dune-istl
# releases/2.6 # 9698e497743654b6a03c219b2bdfc27b62a7e0b3 # 2018-12-14 10:00:30 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.6
git reset --hard 9698e497743654b6a03c219b2bdfc27b62a7e0b3
cd ..

# dune-foamgrid
# releases/2.6 # 975458f8dbcae07a682c03961f5dcc6cf54ebd67 # 2018-07-23 15:25:01 +0000 # Timo Koch
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout releases/2.6
git reset --hard 975458f8dbcae07a682c03961f5dcc6cf54ebd67
cd ..

# dune-alugrid
# releases/2.6 # c0851a92b3af8d93a75f798de3d34f65cc895341 # 2018-07-25 11:01:08 2018 +0000 # Martin Alkämper
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
cd dune-alugrid
git checkout releases/2.6
git reset --hard c0851a92b3af8d93a75f798de3d34f65cc895341
cd ..

# dumux
# master # 1936a09488c207121ce4ec78696c850a246ad931 # 2019-10-14 15:36:11 +0200 # Kilian Weishaupt
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout master
git reset --hard bbd0ffdccacc9a2c3f3c11cdd008a56b67191df9
cd ..

# the pub-module containing the code for the example
git clone https://git.iws.uni-stuttgart.de/dumux-pub/glaeser2020a

# configure project
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
