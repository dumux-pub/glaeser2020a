Welcome to the dumux pub table Glaeser2020a
========================================

This module contains the source code for the example shown in the description
of the open-source fracture network generator [Frackit][1]. <!-- and in the paper __TODO: PUT PAPER CITATION HERE__ -->


Installation instruction (__for linux__)
----------------------------------------

For building the code from source, create a new folder and download the `installGlaeser2020a.sh` script from this repository into the newly created folder. Then, type

```sh
chmod +x installGlaeser2020a.sh
./installGlaeser2020a.sh
```
to download and configure all dune dependencies and dumux. This might take a while. Please note that the following requirements need to be installed:

- CMake (>2.8.12)
- C, C++ compiler (C++14 required)
- Fortran compiler (gfortran)
- UMFPack from SuiteSparse

After successfully running the installation script, navigate to the folder containing the example:

```sh
cd glaeser2020a/build-cmake/example/
```

You can choose between three discretization schemes, namely __tpfa__, __mpfa__ and __box__.
Compile the example with the scheme you want to use, by typing one of the following three
commands into the terminal:

```sh
make example_tpfa
make example_mpfa
make example_box
```

You can now execute the example (for instance using the __box__ scheme), by typing:

```sh
./example_box example.input
```

Have a look at the `example.input` file to see what parameters you can change
without having to modify the source code.

[1]: https://git.iws.uni-stuttgart.de/tools/frackit


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Glaeser2020a
cd Glaeser2020a
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/glaeser2020a/-/raw/master/docker_glaeser2020a.sh
```

Open the Docker Container by running
```bash
bash docker_glaeser2020a.sh open
```

After the script has run successfully, you may build all executables

```bash
cd glaeser2020a/build-cmake
make build_tests
```

and you can run them individually. They are located in the example folder. It can be executed with an input filr, e.g., by running

```bash
cd example
./example_box example.input
```

