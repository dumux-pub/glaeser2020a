// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Example application for the multidomain module
 *        for discrete fracture-matrix simulations in fractured
 *        porous media, using a stochastically generated fracture
 *        network.
 */
#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>

// definitions of the sub-problems
#include "matrixproblem.hh"
#include "fractureproblem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/assembly/diffmethod.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include "computevelocities.hh"

// Define some types for this test so that we can set them as properties below and
// reuse them again in the main function with only one single definition of them here
template<class MatrixTypeTag, class FractureTypeTag>
struct TestTraits
{
    using MatrixFVGridGeometry = Dumux::GetPropType<MatrixTypeTag, Dumux::Properties::GridGeometry>;
    using FractureFVGridGeometry = Dumux::GetPropType<FractureTypeTag, Dumux::Properties::GridGeometry>;
    using TheMultiDomainTraits = Dumux::MultiDomainTraits<MatrixTypeTag, FractureTypeTag>;
    using TheCouplingMapper = Dumux::FacetCouplingMapper<MatrixFVGridGeometry, FractureFVGridGeometry>;
    using TheCouplingManager = Dumux::FacetCouplingManager<TheMultiDomainTraits, TheCouplingMapper>;
};

// get the problem type tags from CMakeLists.txt
using MatrixTypeTag = Dumux::Properties::TTag::MATRIXTYPETAG;
using FractureTypeTag = Dumux::Properties::TTag::FRACTURETYPETAG;

// set the coupling manager property in the sub-problems
namespace Dumux {
namespace Properties {

using Traits = TestTraits<MatrixTypeTag, FractureTypeTag>;

template<class TypeTag>
struct CouplingManager<TypeTag, MatrixTypeTag> { using type = typename Traits::TheCouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, FractureTypeTag> { using type = typename Traits::TheCouplingManager; };

} // end namespace Properties
} // end namespace Dumux

// updates the finite volume grid geometry. The constructor is
// different for the box-scheme, which is why we outsource it here
// from the main routine in order to avoid preprocessor #if blocks
template< class FVGridGeometry,
          class GridManager,
          class LowDimGridView,
          std::enable_if_t<FVGridGeometry::discMethod == Dumux::DiscretizationMethod::box, int> = 0 >
void updateMatrixFVGridGeometry(FVGridGeometry& fvGridGeometry,
                                const GridManager& gridManager,
                                const LowDimGridView& lowDimGridView)
{
    using BulkFacetGridAdapter = Dumux::CodimOneGridAdapter<typename GridManager::Embeddings>;
    BulkFacetGridAdapter facetGridAdapter(gridManager.getEmbeddings());
    fvGridGeometry.update(lowDimGridView, facetGridAdapter);
}

// specialization for cell-centered schemes
template< class FVGridGeometry,
          class GridManager,
          class LowDimGridView,
          std::enable_if_t<FVGridGeometry::discMethod != Dumux::DiscretizationMethod::box, int> = 0 >
void updateMatrixFVGridGeometry(FVGridGeometry& fvGridGeometry,
                                const GridManager& gridManager,
                                const LowDimGridView& lowDimGridView)
{
    fvGridGeometry.update();
}

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // Create the grid
    using MatrixGrid = GetPropType<MatrixTypeTag, Properties::Grid>;
    using FractureGrid = GetPropType<FractureTypeTag, Properties::Grid>;
    using GridManager = Dumux::FacetCouplingGridManager<MatrixGrid, FractureGrid>;
    GridManager gridManager;
    gridManager.init();
    gridManager.loadBalance();

    // we compute on the leaf grid views (get them from grid manager)
    // the grid ids correspond to the order of the grids passed to the manager (see above)
    static constexpr std::size_t matrixGridId = 0;
    static constexpr std::size_t fractureGridId = 1;
    const auto& matrixGridView = gridManager.template grid<matrixGridId>().leafGridView();
    const auto& fractureGridView = gridManager.template grid<fractureGridId>().leafGridView();

    // create the finite volume grid geometries
    using TestTraits = TestTraits<MatrixTypeTag, FractureTypeTag>;
    using MatrixFVGridGeometry = typename TestTraits::MatrixFVGridGeometry;
    using FractureFVGridGeometry = typename TestTraits::FractureFVGridGeometry;
    auto matrixFvGridGeometry = std::make_shared<MatrixFVGridGeometry>(matrixGridView);
    auto fractureFvGridGeometry = std::make_shared<FractureFVGridGeometry>(fractureGridView);
    updateMatrixFVGridGeometry(*matrixFvGridGeometry, gridManager, fractureGridView);
    fractureFvGridGeometry->update();

    // the problems (boundary/initial conditions etc)
    using MatrixProblem = GetPropType<MatrixTypeTag, Properties::Problem>;
    using FractureProblem = GetPropType<FractureTypeTag, Properties::Problem>;

    auto matrixGridData = gridManager.getGridData()->template getSubDomainGridData<matrixGridId>();
    auto matrixSpatialParams = std::make_shared<typename MatrixProblem::SpatialParams>(matrixFvGridGeometry, matrixGridData, "Matrix");
    auto matrixProblem = std::make_shared<MatrixProblem>(matrixFvGridGeometry, matrixSpatialParams, "Matrix");

    auto fractureGridData = gridManager.getGridData()->template getSubDomainGridData<fractureGridId>();
    auto fractureSpatialParams = std::make_shared<typename FractureProblem::SpatialParams>(fractureFvGridGeometry, fractureGridData, "Fracture");
    auto fractureProblem = std::make_shared<FractureProblem>(fractureFvGridGeometry, fractureSpatialParams, "Fracture");

    // the solution vector
    using SolutionVector = typename TestTraits::TheMultiDomainTraits::SolutionVector;
    SolutionVector x;

    // The domain ids within the multi-domain framework.
    static const auto matrixDomainId = typename TestTraits::TheMultiDomainTraits::template SubDomain<0>::Index();
    static const auto fractureDomainId = typename TestTraits::TheMultiDomainTraits::template SubDomain<1>::Index();

    // instantiate the class holding the coupling maps between the domains
    const auto embeddings = gridManager.getEmbeddings();
    auto couplingMapper = std::make_shared<typename TestTraits::TheCouplingMapper>();
    couplingMapper->update(*matrixFvGridGeometry, *fractureFvGridGeometry, embeddings);

    // the coupling manager (needs the coupling mapper)
    auto couplingManager = std::make_shared<typename TestTraits::TheCouplingManager>();
    couplingManager->init(matrixProblem, fractureProblem, couplingMapper, x);

    // we have to set coupling manager pointer in sub-problems
    // they also have to be made accessible in them (see e.g. matrixproblem.hh)
    matrixProblem->setCouplingManager(couplingManager);
    fractureProblem->setCouplingManager(couplingManager);

    // resize the solution vector and write initial solution to it
    x[matrixDomainId].resize(matrixFvGridGeometry->numDofs());
    x[fractureDomainId].resize(fractureFvGridGeometry->numDofs());
    matrixProblem->applyInitialSolution(x[matrixDomainId]);
    fractureProblem->applyInitialSolution(x[fractureDomainId]);

    // the grid variables
    using MatrixGridVariables = GetPropType<MatrixTypeTag, Properties::GridVariables>;
    using FractureGridVariables = GetPropType<FractureTypeTag, Properties::GridVariables>;
    auto matrixGridVariables = std::make_shared<MatrixGridVariables>(matrixProblem, matrixFvGridGeometry);
    auto fractureGridVariables = std::make_shared<FractureGridVariables>(fractureProblem, fractureFvGridGeometry);
    matrixGridVariables->init(x[matrixDomainId]);
    fractureGridVariables->init(x[fractureDomainId]);

    // intialize the vtk output modules
    using MatrixVtkOutputModule = VtkOutputModule<MatrixGridVariables, GetPropType<MatrixTypeTag, Properties::SolutionVector>>;
    using FractureVtkOutputModule = VtkOutputModule<FractureGridVariables, GetPropType<FractureTypeTag, Properties::SolutionVector>>;
    const auto matrixDM = MatrixFVGridGeometry::discMethod == DiscretizationMethod::box ? Dune::VTK::nonconforming : Dune::VTK::conforming;
    MatrixVtkOutputModule matrixVtkWriter(*matrixGridVariables, x[matrixDomainId], matrixProblem->name(), "Matrix", matrixDM);
    FractureVtkOutputModule fractureVtkWriter(*fractureGridVariables, x[fractureDomainId], fractureProblem->name(), "Fracture");

    // Add model specific output fields
    using MatrixIOFields = GetPropType<MatrixTypeTag, Properties::IOFields>;
    using FractureIOFields = GetPropType<FractureTypeTag, Properties::IOFields>;
    MatrixIOFields::initOutputModule(matrixVtkWriter);
    FractureIOFields::initOutputModule(fractureVtkWriter);

    // add domain markers to output
    std::vector<int> matrixDomainMarkers(matrixFvGridGeometry->gridView().size(0));
    for (const auto& element : elements(matrixFvGridGeometry->gridView()))
        matrixDomainMarkers[matrixFvGridGeometry->elementMapper().index(element)] = matrixProblem->spatialParams().getElementDomainMarker(element);
    matrixVtkWriter.addField(matrixDomainMarkers, "domainMarker");

    std::vector<int> fractureDomainMarkers(fractureFvGridGeometry->gridView().size(0));
    for (const auto& element : elements(fractureFvGridGeometry->gridView()))
        fractureDomainMarkers[fractureFvGridGeometry->elementMapper().index(element)] = fractureProblem->spatialParams().getElementDomainMarker(element);
    fractureVtkWriter.addField(fractureDomainMarkers, "domainMarker");

    // add velocities to the output
    using Velocity = Dune::FieldVector<double, 3>;
    using VelocityVector = std::vector<Velocity>;

    VelocityVector velocityMatrix(matrixFvGridGeometry->gridView().size(0));
    VelocityVector velocityFracture(fractureFvGridGeometry->gridView().size(0));

    matrixVtkWriter.addField(velocityMatrix, "velocity");
    fractureVtkWriter.addField(velocityFracture, "velocity");

    // the assembler for the coupled problem
    using Assembler = MultiDomainFVAssembler<typename TestTraits::TheMultiDomainTraits,
                                             typename TestTraits::TheCouplingManager,
                                             DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(matrixProblem, fractureProblem),
                                                  std::make_tuple(matrixFvGridGeometry, fractureFvGridGeometry),
                                                  std::make_tuple(matrixGridVariables, fractureGridVariables),
                                                  couplingManager);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, typename TestTraits::TheCouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

    couplingManager->updateSolution(x);
    computeVelocities<MatrixTypeTag>(matrixDomainId, *assembler, *couplingManager, *matrixFvGridGeometry, *matrixGridVariables, x[matrixDomainId], velocityMatrix);
    computeVelocities<FractureTypeTag>(fractureDomainId, *assembler, *couplingManager, *fractureFvGridGeometry, *fractureGridVariables, x[fractureDomainId], velocityFracture);

    // write out initial solution
    matrixVtkWriter.write(0.0);
    fractureVtkWriter.write(0.0);

    // solve system and write output
    newtonSolver->solve(x);

    matrixGridVariables->update(x);
    fractureGridVariables->update(x);

    computeVelocities<MatrixTypeTag>(matrixDomainId, *assembler, *couplingManager, *matrixFvGridGeometry, *matrixGridVariables, x[matrixDomainId], velocityMatrix);
    computeVelocities<FractureTypeTag>(fractureDomainId, *assembler, *couplingManager, *fractureFvGridGeometry, *fractureGridVariables, x[fractureDomainId], velocityFracture);

    matrixVtkWriter.write(1.0);
    fractureVtkWriter.write(1.0);

    // print dumux message to say goodbye
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    return 0;

}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
