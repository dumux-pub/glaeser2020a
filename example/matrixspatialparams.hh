// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial parameters for the fracture sub-domain in the
 *        example on single-phase flow in fractured porous media.
 */
#ifndef DUMUX_MATRIX_SPATIALPARAMS_HH
#define DUMUX_MATRIX_SPATIALPARAMS_HH

#include <dumux/io/grid/griddata.hh>
#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

/*!
 * \brief The spatial params the two-phase facet coupling test
 */
template< class FVGridGeometry, class Scalar >
class MatrixSpatialParams
: public FVSpatialParamsOneP< FVGridGeometry, Scalar, MatrixSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = MatrixSpatialParams< FVGridGeometry, Scalar >;
    using ParentType = FVSpatialParamsOneP< FVGridGeometry, Scalar, ThisType >;

    using GridView = typename FVGridGeometry::GridView;
    using Grid = typename GridView::Grid;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! the constructor
    MatrixSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                        std::shared_ptr<const Dumux::GridData<Grid>> gridData,
                        const std::string& paramGroup)
    : ParentType(fvGridGeometry)
    , gridDataPtr_(gridData)
    {
        porosity1_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Porosity1");
        porosity2_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Porosity2");
        porosity3_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Porosity3");

        permeability1_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability1");
        permeability2_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability2");
        permeability3_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability3");
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    template<class ElemSol>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElemSol& elemSol) const
    {
        int marker = getElementDomainMarker(element);
        if (marker == 1) return permeability1_;
        else if (marker == 2) return permeability2_;
        else if (marker == 3) return permeability3_;
        else DUNE_THROW(Dune::InvalidStateException, "Unexpected marker");
    }

    //! Return the porosity
    template<class ElemSol>
    PermeabilityType porosity(const Element& element,
                              const SubControlVolume& scv,
                              const ElemSol& elemSol) const
    {
        int marker = getElementDomainMarker(element);
        if (marker == 1) return porosity1_;
        else if (marker == 2) return porosity2_;
        else if (marker == 3) return porosity3_;
        else DUNE_THROW(Dune::InvalidStateException, "Unexpected marker");
    }

    //! returns the domain marker for an element
    int getElementDomainMarker(const Element& element) const
    { return gridDataPtr_->getElementDomainMarker(element); }

private:
    //! pointer to the grid data (contains domain markers)
    std::shared_ptr<const Dumux::GridData<Grid>> gridDataPtr_;

    Scalar porosity1_;
    Scalar porosity2_;
    Scalar porosity3_;
    PermeabilityType permeability1_;
    PermeabilityType permeability2_;
    PermeabilityType permeability3_;
};

} // end namespace Dumux

#endif
