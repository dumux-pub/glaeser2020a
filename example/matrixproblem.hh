// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The sub-problem for the matrix domain in the
 *        example on single-phase flow in fractured porous media.
 */
#ifndef DUMUX_MATRIX_PROBLEM_HH
#define DUMUX_MATRIX_PROBLEM_HH

#include <dune/alugrid/grid.hh>
#include <dune/common/indices.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>
#include <dumux/multidomain/facet/box/properties.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>
#include "matrixspatialparams.hh"

namespace Dumux {

// forward declaration of the problem class
template<class TypeTag> class MatrixSubProblem;

namespace Properties {

// create the type tag node
namespace TTag {
struct MatrixProblem { using InheritsFrom = std::tuple<OneP>; };
struct MatrixProblemTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, MatrixProblem>; };
struct MatrixProblemMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, MatrixProblem>; };
struct MatrixProblemBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, MatrixProblem>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::MatrixProblem> { using type = Dune::ALUGrid<3, 3, Dune::simplex, Dune::conforming>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::MatrixProblem> { using type = MatrixSubProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::MatrixProblem>
{
    using type = MatrixSpatialParams< GetPropType<TypeTag, Properties::GridGeometry>,
                                      GetPropType<TypeTag, Properties::Scalar> >;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::MatrixProblem>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Comp = Dumux::Components::Constant<0, Scalar>;

public:
    using type = Dumux::FluidSystems::OnePLiquid< Scalar, Comp >;
};

} // end namespace Properties

/*!
 * \brief The sub-problem for the matrix domain in the
 *        example on single-phase flow in fractured porous media.
 */
template<class TypeTag>
class MatrixSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    //! The constructor
    MatrixSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    {}

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        if (isOnInlet_(globalPos) || isOnOutlet_(globalPos))
            values.setAllDirichlet();
        return values;
    }

    //! Specifies the type of interior boundary condition to be used on a sub-control volume face
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        values.setAllDirichlet();
        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        // initialize values with the initial conditions
        auto values = initialAtPos(globalPos);

        // increase pressure on inlet
        if (isOnInlet_(globalPos))
        {
            static const auto overPressure = getParam<double>("Problem.InjectionOverPressure");
            values[0] += overPressure;
        }

        return values;
    }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        const auto domainHeight = this->gridGeometry().bBoxMax()[2];
        const auto& g = this->spatialParams().gravity(globalPos);
        static const auto rho = getParam<Scalar>("0.Component.LiquidDensity");

        PrimaryVariables values;
        values[0] = 1.0e5 - (domainHeight - globalPos[2])*rho*g[2];

        return values;
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

private:
    bool isOnLeftBoundary_(const GlobalPosition& globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + 1e-6; }

    bool isOnRightBoundary_(const GlobalPosition& globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - 1e-6; }

    bool isOnInlet_(const GlobalPosition& globalPos) const
    { return isOnLeftBoundary_(globalPos) && globalPos[2] < 25.0 + 1e-5; }

    bool isOnOutlet_(const GlobalPosition& globalPos) const
    { return isOnRightBoundary_(globalPos) && globalPos[2] > 55.0 - 1e-5; }

    std::shared_ptr<CouplingManager> couplingManagerPtr_;
};

} // end namespace Dumux

#endif
