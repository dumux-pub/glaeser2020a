dune_symlink_to_source_files(FILES "grids" "example.input")

dune_add_test(NAME example_tpfa
              SOURCES example.cc
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )"
              COMPILE_DEFINITIONS MATRIXTYPETAG=MatrixProblemTpfa
              COMPILE_DEFINITIONS FRACTURETYPETAG=FractureProblemTpfa
              COMMAND ./example_tpfa
              CMD_ARGS example.input -Matrix.Problem.Name tpfa_matrix -Fracture.Problem.Name tpfa_fracture)

dune_add_test(NAME example_mpfa
              SOURCES example.cc
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )"
              COMPILE_DEFINITIONS MATRIXTYPETAG=MatrixProblemMpfa
              COMPILE_DEFINITIONS FRACTURETYPETAG=FractureProblemMpfa
              COMMAND ./example_mpfa
              CMD_ARGS example.input -Matrix.Problem.Name mpfa_matrix -Fracture.Problem.Name mpfa_fracture)

dune_add_test(NAME example_box
              SOURCES example.cc
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )"
              COMPILE_DEFINITIONS MATRIXTYPETAG=MatrixProblemBox
              COMPILE_DEFINITIONS FRACTURETYPETAG=FractureProblemBox
              COMMAND ./example_box
              CMD_ARGS example.input -Matrix.Problem.Name box_matrix -Fracture.Problem.Name box_fracture)

set(CMAKE_BUILD_TYPE Release)
